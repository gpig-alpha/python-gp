import random
import operator
import numpy
import pickle
from functools import partial
import itertools
import TCPServer

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

pset = gp.PrimitiveSetTyped("MAIN",[float, float, float, float, float, float, float, float, float], float )
#pset.addPrimitive(operator.and_, [bool, bool], bool)
#pset.addPrimitive(operator.or_, [bool, bool], bool)
#pset.addPrimitive(operator.not_, [bool], bool)

def protectedDiv(left, right):
    try: return left / right
    except ZeroDivisionError: return 1

pset.addPrimitive(operator.add, [float,float], float)
pset.addPrimitive(operator.sub, [float,float], float)
pset.addPrimitive(operator.mul, [float,float], float)
pset.addPrimitive(protectedDiv, [float,float], float)
pset.renameArguments(ARG0='priority')
pset.renameArguments(ARG1='maxSpeed')
pset.renameArguments(ARG2='pollutionGrade')
pset.renameArguments(ARG3='numberOfLanes')
pset.renameArguments(ARG4='distance')
pset.renameArguments(ARG5='speedLimit')
pset.renameArguments(ARG6='conjestion')
pset.renameArguments(ARG7='cleanFactor')
pset.renameArguments(ARG8='open')

def if_then_else(input, output1, output2):
    if input: return output1
    else: return output2

#pset.addPrimitive(operator.lt, [float, float], bool)
#pset.addPrimitive(operator.eq, [float, float], bool)
#pset.addPrimitive(if_then_else, [bool, float, float], float)

pset.addEphemeralConstant("rand100", lambda: random.random() * 100, float)
#pset.addTerminal(False, bool)
#pset.addTerminal(True, bool)

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=1, max_=3)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("compile", gp.compile, pset=pset)

def evalind(individual):
    func = toolbox.compile(expr=individual)
    result=0

    conn.send("getIn")
    NumberOfCars = int(conn.recieve())
    conn.send("start")
    for i in range(NumberOfCars):
        val=""
        while True:
            val=conn.recieve()
            if val=="NOMORE":
                break;
            inputs = [float(x) for x in val.split(',') ]
            priority = inputs[0]
            maxSpeed = inputs[1]
            pollutionGrade = inputs[2]
            numberOfLanes = inputs[3]
            distance = inputs[4]
            speedLimit = inputs[5]
            conjestion = inputs[6]
            cleanFactor = inputs[7]
            open = inputs[8]
            conn.send(str(func(priority, maxSpeed, pollutionGrade, numberOfLanes,  distance, speedLimit, conjestion, cleanFactor, open)))
        conn.send("FitPls")
        result  += float(conn.recieve())
        conn.send("Recieved")
    print("Done")


    
    
    

    
    return result, 
    
toolbox.register("evaluate", evalind)
toolbox.register("select", tools.selDoubleTournament, fitness_size=5,parsimony_size=1.3,fitness_first=True)
toolbox.register("mate", gp.cxOnePoint)
toolbox.register("expr_mut", gp.genHalfAndHalf, min_=0, max_=3)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

toolbox.decorate("mate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))
toolbox.decorate("mutate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))


def main():
    random.seed()
    size=100
    NGEN=30
    CXPB=0.5
    MUTPB=0.2
    pop=toolbox.population(n=size)
    start_gen=0
    hof=tools.HallOfFame(1)
    logb=tools.Logbook()


    stats_fit=tools.Statistics(lambda ind:ind.fitness.values)
    stats_size=tools.Statistics(key=operator.attrgetter("height"))
    stats=tools.MultiStatistics(fitness=stats_fit,size=stats_size)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)
    logb.header="gen","evals","fitness","size"
    logb.chapters["fitness"].header="avg","std","min","max"
    logb.chapters["size"].header="avg","std","min","max"
    
    #algorithms.eaSimple(pop, toolbox, CXPB, MUTPB, NGEN, stats, halloffame=hof)
    gen=0
    while( gen in range(start_gen,NGEN+1)) : #and (gen<3 or float(hof[0].fitness.values[0])!=10))
        pop = toolbox.select(pop, len(pop))
        offspring=algorithms.varAnd(pop,toolbox,cxpb=CXPB,mutpb=MUTPB)#changr to offspring
        #invalid_ind=[ind for ind in offspring if not ind.fitness.valid] # for minimal evaluation
        invalid_ind=[ind for ind in offspring ] # for best evaluation
        fitnesses=toolbox.map(toolbox.evaluate,invalid_ind)
        for ind, fit in zip(invalid_ind,fitnesses):
            ind.fitness.values=fit
        pop=offspring
        hof.update(pop)
        record=stats.compile(pop)
        
        logb.record(gen=gen,evals=len(invalid_ind),**record)
        print(logb.stream)
        gen +=1
        conn.send("GEN")
        conn.recieve()
        conn.send("OK")
        
        

    conn.send("END")
    conn.end()
    tree=gp.PrimitiveTree(hof[0])
    print(str(tree))
    with open("Individual", "wb") as file:
        pickle.dump(hof[0], file)
    

    return pop, stats, hof
if __name__ == "__main__":
    conn=TCPServer.Connection()
    main()
    
