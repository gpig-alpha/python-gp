package DBReader;

import java.util.Map.Entry;

import SimpleRoadNetwok.CarData;
import SimpleRoadNetwok.Dijkstra;
import SimpleRoadNetwok.Graph;
import SimpleRoadNetwok.Node;
import SimpleRoadNetwok.RoadData;

public class Test {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("fetching");
		DB db = new DB();
		System.out.println("fetched");
		
		Graph example = db.GenerateGraph(new CarData("Test",10,120,5));
		example.setStartNodeName("257923782");
		example.setEndNodeName("262974100");
		System.out.println(db.roadDatas.size());
		System.out.println(db.nodes.size());
		
		Graph enviro = example.clone();
		for (Node n: enviro.getNodes())
		{
			for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
			{
				RoadData road = roadPairs.getValue();
				double weight = Graph.simpleCalcEnviro(n, roadPairs.getKey(), enviro.getCar());
				System.out.println(weight);
				n.addDestination(roadPairs.getKey(), weight);
			}
			
		}
		Node enviroStart = enviro.getNodeByName(enviro.getStartNodeName());
		Graph enviroCalc = Dijkstra.calculateShortestPathFromSource(enviro,enviroStart );
		
		Graph distance = example.clone();
		for (Node n: distance.getNodes())
		{
			for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
			{
				RoadData road = roadPairs.getValue();
				double weight = Graph.simpleCalcDistance(n, roadPairs.getKey(), distance.getCar());
				n.addDestination(roadPairs.getKey(), weight);
			}
			
		}
		Node distanceStart = distance.getNodeByName(distance.getStartNodeName());
		Graph distanceCalc = Dijkstra.calculateShortestPathFromSource(distance,distanceStart );
		
		
		System.out.println("EnviroCalc");
		for(Node n: enviroCalc.getNodes())
		{
			if(enviroCalc.getEndNodeName().equals(n.getName())) {
			System.out.println(n.getName());
			System.out.println(n.getDistance());
			System.out.println(n.getEfficency());
			System.out.println(n.getEnviro());
			System.out.println(n.getShortestPath());
			for (Node a: n.getShortestPath())
			{
				System.out.println(a.getName()+" : "+db.GetLatLonOfNode(a.getName()).toString());
			}
			System.out.println(n.getName()+" : "+db.GetLatLonOfNode(n.getName()).toString());
			}
		}
		System.out.println();
		System.out.println("DistanceCalc");
		for(Node n: distanceCalc.getNodes())
		{
			if(enviroCalc.getEndNodeName().equals(n.getName())) {
			System.out.println(n.getName());
			System.out.println(n.getDistance());
			System.out.println(n.getEfficency());
			System.out.println(n.getEnviro());
			System.out.println(n.getShortestPath());
			for (Node a: n.getShortestPath())
			{
				System.out.println(a.getName()+" : "+db.GetLatLonOfNode(a.getName()).toString());
			}
			System.out.println(n.getName()+" : "+db.GetLatLonOfNode(n.getName()).toString());
			}
		}
		


	}

}
