package DBReader;

import SimpleRoadNetwok.*;

public class Road {
	
	private RoadData myData;
	private String StartNodeID;
	private String EndNodeID;
	
	public Road(String Start, String End, RoadData roadData)
	{
		this.StartNodeID = Start;
		this.EndNodeID = End;
		this.myData = roadData;
	}

	public RoadData getMyData() {
		return myData;
	}

	public void setMyData(RoadData myData) {
		this.myData = myData;
	}

	public String getStartNodeID() {
		return StartNodeID;
	}

	public void setStartNodeID(String startNodeID) {
		StartNodeID = startNodeID;
	}

	public String getEndNodeID() {
		return EndNodeID;
	}

	public void setEndNodeID(String endNodeID) {
		EndNodeID = endNodeID;
	}
	

}
