package SimpleRoadNetwok;

import java.util.*;

public class ExampleCarDatas 
{
	
	public LinkedList<CarData> cars = new LinkedList<CarData>();
	
	public ExampleCarDatas()
	{
		cars.add(new CarData("1",100,120,5));
		cars.add(new CarData("2",10,120,2));
		cars.add(new CarData("3",10,260,10));
		cars.add(new CarData("4",50,120,7));
		cars.add(new CarData("5",30,30,8));
		cars.add(new CarData("6",10,120,6));
		cars.add(new CarData("7",10,120,9));
		cars.add(new CarData("8",10,120,3));
		cars.add(new CarData("9",10,120,4));
		cars.add(new CarData("10",20,30,1));
		
	}

}
