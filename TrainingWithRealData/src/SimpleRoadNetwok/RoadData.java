package SimpleRoadNetwok;

public class RoadData 
{
	private int numberOfLanes;
	private float distance;
	private int speedLimit;
	private float conjestion;
	private float cleanFactor;
	private boolean open;
	
	public RoadData()
	{
		
	}
	
	public RoadData(int numberOfLanes, float distance, int speedLimit, float conjestion,float cleanFactor, boolean open)
	{
		this.numberOfLanes=numberOfLanes;
		this.distance=distance;
		this.speedLimit=speedLimit;
		this.conjestion=conjestion;
		this.cleanFactor=cleanFactor;
		this.open=open;
	}

	public int getNumberOfLanes() {
		return numberOfLanes;
	}

	public void setNumberOfLanes(int numberOfLanes) {
		this.numberOfLanes = numberOfLanes;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public int getSpeedLimit() {
		return speedLimit;
	}

	public void setSpeedLimit(int speedLimit) {
		this.speedLimit = speedLimit;
	}

	public float getConjestion() {
		return conjestion;
	}

	public void setConjestion(float conjestion) {
		this.conjestion = conjestion;
	}

	public float getCleanFactor() {
		return cleanFactor;
	}

	public void setCleanFactor(float cleanFactor) {
		this.cleanFactor = cleanFactor;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public String toString() {
		int openint = open?1:0;
		return  numberOfLanes + "," + distance + "," + speedLimit
				+ "," + conjestion + "," + cleanFactor + "," + openint;
	}
	
	public RoadData clone()
	{
		return new RoadData(numberOfLanes,distance,speedLimit,conjestion,cleanFactor,open);
	}
	
	

}
