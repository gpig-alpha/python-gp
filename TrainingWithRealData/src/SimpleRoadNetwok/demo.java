package SimpleRoadNetwok;

import java.util.Map.Entry;

import Communication.GPData;

public class demo {
	
	
	public static void main(String[] args) {
		Graph graph = new ExampleGraph(new CarData("testCar",10,30,4),0);
		Graph graph2 = graph.clone();
		
		for (Node n: graph.getNodes())
		{
			for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
			{
				RoadData road = roadPairs.getValue();
				double weight = Graph.simpleCalcDistance(n, roadPairs.getKey(), graph.getCar());
				n.addDestination(roadPairs.getKey(), (int)weight);
			}
			
		}
		
		for (Node n: graph2.getNodes())
		{
			for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
			{
				RoadData road = roadPairs.getValue();
				double weight = Graph.simpleCalcEnviro(n, roadPairs.getKey(), graph2.getCar());
				n.addDestination(roadPairs.getKey(), (int)weight);
			}
			
		}
		
		
		Node start = null;
		for (Node n: graph.getNodes())
		{
			if (n.getName().equals("START"))
				start=n;
		
		}
		Graph calc = Dijkstra.calculateShortestPathFromSource(graph,start );
		for (Node n : calc.getNodes())
		{
			System.out.println(n.getName());
			System.out.println(n.getDistance());
			System.out.println(n.getEfficency());
			System.out.println(n.getEnviro());
			System.out.println(n.getShortestPath());
		}
		
		System.out.println("COPY TEST");
		Node start2 = null;
		for (Node n: graph2.getNodes())
		{
			if (n.getName().equals("START"))
				start2=n;
		
		}
		
		Graph calc2 = Dijkstra.calculateShortestPathFromSource(graph2,start2 );
		for (Node n : calc2.getNodes())
		{
			System.out.println(n.getName());
			System.out.println(n.getDistance());
			System.out.println(n.getEfficency());
			System.out.println(n.getEnviro());
			System.out.println(n.getShortestPath());
		}
	}

}
