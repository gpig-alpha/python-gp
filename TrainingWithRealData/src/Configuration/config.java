package Configuration;

import java.util.Map.Entry;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

import DBReader.Road;
import SimpleRoadNetwok.CarData;
import SimpleRoadNetwok.Node;
import SimpleRoadNetwok.RoadData;

public class config {
	
	//The amount the weight should reward the environment over efficiency of path: value between 0 and 1
	public static float enviromentFactor = 1f;
	
	private static Random r = new Random();
	
	//The basic calculation to determine efficiency
	public static double simpleCalcDistance(Node source,Node target,CarData car)
	{
		double value = Double.MAX_VALUE;
		for (Entry<Node,RoadData> road:source.getRoadDatas().entrySet())
		{
			if (road.getKey().getName().equals(target.getName()))
			{
				
				value =  ((double)road.getValue().getDistance()/(Math.min(road.getValue().getSpeedLimit(),car.getMaxSpeed())) + 0.1*(road.getValue().getDistance()*road.getValue().getConjestion()/road.getValue().getNumberOfLanes()) + 0.3 * road.getValue().getDistance()* (car.getMaxSpeed()<road.getValue().getSpeedLimit()? 1 : 0));
				
				if (!road.getValue().isOpen())
					value=(int) Double.MAX_VALUE/2;

			}
		}
		return value;
	}
	
	//The basic calculation to determine envriomental impact
	public static double simpleCalcEnviro(Node source,Node target,CarData car)
	{
		double value = Double.MAX_VALUE/2f;
		for (Entry<Node,RoadData> road:source.getRoadDatas().entrySet())
		{
			if (road.getKey().getName().equals(target.getName()))
			{
				value =  (Math.max((road.getValue().getConjestion()/10)*5 + car.getPollutionGrade()*10 + road.getValue().getCleanFactor()*road.getValue().getCleanFactor() - 100 ,1));
				return value;
			}
		}
		System.err.println("Not connected");
		return value;
	}
	
public static Road GenerateRoad(JSONArray array, int index) {
	
	
	Object line = JSONValue.parse(array.get(index).toString());
	JSONArray arrayInner = (JSONArray) line;
	
	String highWay = arrayInner.get(11).toString();
	int highWayValue=getHighWayValue(highWay);
	
	String sideWalk = arrayInner.get(12).toString();
	int sideWalkValue=getSideWalkValue(sideWalk);
	
	float buildingDensity = 10*Float.parseFloat(arrayInner.get(7).toString())/127760;
	
	int congestion = (int) ((int)(10-highWayValue)*r.nextFloat()*10);
	int enviroment = (int) (buildingDensity*(highWayValue+sideWalkValue));
	//					Start Node						END Node									max speed												road length									speedlimit							congestion	envirofactor			open
	return new Road(arrayInner.get(2).toString(), arrayInner.get(4).toString(), new RoadData(Integer.parseInt(arrayInner.get(9).toString()),Float.parseFloat(arrayInner.get(8).toString()),Integer.parseInt(arrayInner.get(10).toString()),congestion,enviroment,true));
}
	
	
	

	private static int getHighWayValue(String highWay)
	{
		int highWayValue = 0;
		switch (highWay)
		{
		case "residential":
			highWayValue=8;
			break;
		case "living_street":
			highWayValue=6;
			break;
		case "tertiary":
			highWayValue=4;
			break;
		case "primary_link":
			highWayValue=2;
			break;
		case "primary":
			highWayValue=1;
			break;
			
		}
		return highWayValue;
	
	}
	
	private static int getSideWalkValue(String sideWalk)
	{
		int sideWalkValue =0;
		switch(sideWalk)
		{
		case "left":
			sideWalkValue=1;
			break;
		case "right":
			sideWalkValue=1;
			break;
		case "both":
			sideWalkValue=2;
			break;
		}
		
		return sideWalkValue;
	}

}
