package Communication;


import java.io.*;
import java.net.*;
import java.util.*;
import java.util.Map.Entry;

import SimpleRoadNetwok.*;
import DBReader.*;
import Configuration.*;

import java.math.*;

public class test {
// Evaluates the performance of the individual connected to python program

	public static void main(String[] args) throws Exception{
		TCPClient2 client = new TCPClient2(InetAddress.getByName("localhost"),5000);
		System.out.println("\r\n Connected to Server: "+client.socket.getInetAddress());
		Random r = new Random();
		LinkedList<GPData> d = new LinkedList<GPData>() ;
		DB db = new DB();
		Graph example= db.GenerateGraph(new CarData("Shouldn't be used",0,0,0));
		LinkedList<Graph> cases = new LinkedList<Graph>();
	
	
		//Make a way to generate data
		List<String> nodeNames = new ArrayList<String>();
		for(Node n :example.getNodes())
		{
			nodeNames.add(n.getName());
		}
		
		ExampleCarDatas cars= new ExampleCarDatas();

		int inter = 0;
		while (client.connected)
		{
			
			

			String val=client.recieve();
			
			if (val.compareTo("getIn")==0)
			{
				
					Graph e=cases.get(inter%cases.size());
					inter+=1;
					example=  e.clone();
					Graph distance = example.clone();
					for (Node n: distance.getNodes())
					{
						for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
						{
							RoadData road = roadPairs.getValue();
							double weight = config.simpleCalcDistance(n, roadPairs.getKey(), distance.getCar());
							n.addDestination(roadPairs.getKey(), weight);
						}
						
					}
					Node distanceStart = distance.getNodeByName(distance.getStartNodeName());
					Graph distanceCalc = Dijkstra.calculateShortestPathFromSource(distance,distanceStart );
					double distanceVal = Double.MAX_VALUE;
					for(Node n:distanceCalc.getNodes())
						if(n.getName().equals(distance.getEndNodeName()))
							distanceVal = n.getEfficency();
					//System.out.println(distanceVal);
					
					Graph enviro = example.clone();
					for (Node n: enviro.getNodes())
					{
						for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
						{
							RoadData road = roadPairs.getValue();
							double weight = config.simpleCalcEnviro(n, roadPairs.getKey(), enviro.getCar());
							n.addDestination(roadPairs.getKey(), weight);
						}
						
					}
					Node enviroStart = enviro.getNodeByName(enviro.getStartNodeName());
					Graph enviroCalc = Dijkstra.calculateShortestPathFromSource(enviro,enviroStart );
					double enviroVal = Double.MAX_VALUE;
					for(Node n:enviroCalc.getNodes())
						if(n.getName().equals(enviro.getEndNodeName()))
							enviroVal = n.getEnviro();
					//System.out.println(enviroVal);
					
					example= e.clone();
					double fitness = 0;
					
					LinkedList<RoadData> roads = new LinkedList<RoadData>();
					for (Node n: example.getNodes())
					{
						for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
						{
							RoadData road = roadPairs.getValue();
							roads.add(road);
							//d.add(new GPData(e.getCar(),road));
							String message = String.valueOf(e.getCar().toString())+","+String.valueOf(road.toString());
							client.send(message);
							double weight = Float.parseFloat(client.recieve());
							//System.out.println("weight"+weight);
							n.addDestination(roadPairs.getKey(), weight);
						}
						
					}
					client.send("NOMORE");	
					example =  Dijkstra.calculateShortestPathFromSource(example, example.getNodeByName(example.getStartNodeName()));
					double efficiencyOfPath = example.getNodeByName(example.getEndNodeName()).getEfficency();
					//System.out.println("EfficiencyGoal:"+distanceVal);
					//System.out.println("efficiency: "+efficiencyOfPath);
					double enviroCostOfPath = example.getNodeByName(example.getEndNodeName()).getEnviro();
					//System.out.println("EnviroGoal: "+enviroVal);
					//System.out.println("enviro: "+enviroCostOfPath);
					
					double efficiencyDiff =distanceVal/efficiencyOfPath;
					double enviroDiff =  enviroVal/enviroCostOfPath;
					//System.out.println("EfficiencyDiff"+efficiencyDiff + " EnviroDiff:"+enviroDiff);
					
					fitness+= (1-config.enviromentFactor)*efficiencyDiff + config.enviromentFactor*enviroDiff;
					//System.out.println(fitness);
					client.recieve();
					client.send(String.valueOf(fitness));
					client.recieve();
				
			}
			else if(val.compareTo("getEf")==0)
			{
				Graph e=cases.get(inter%cases.size());
				inter+=1;
				example=  e.clone();
				Graph distance = example.clone();
				for (Node n: distance.getNodes())
				{
					for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
					{
						RoadData road = roadPairs.getValue();
						double weight = config.simpleCalcDistance(n, roadPairs.getKey(), distance.getCar());
						n.addDestination(roadPairs.getKey(), weight);
					}
					
				}
				Node distanceStart = distance.getNodeByName(distance.getStartNodeName());
				Graph distanceCalc = Dijkstra.calculateShortestPathFromSource(distance,distanceStart );
				double distanceVal = Double.MAX_VALUE;
				for(Node n:distanceCalc.getNodes())
					if(n.getName().equals(distance.getEndNodeName()))
						distanceVal = n.getEfficency();
				//System.out.println(distanceVal);
				
				Graph enviro = example.clone();
				for (Node n: enviro.getNodes())
				{
					for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
					{
						RoadData road = roadPairs.getValue();
						double weight = config.simpleCalcEnviro(n, roadPairs.getKey(), enviro.getCar());
						n.addDestination(roadPairs.getKey(), weight);
					}
					
				}
				Node enviroStart = enviro.getNodeByName(enviro.getStartNodeName());
				Graph enviroCalc = Dijkstra.calculateShortestPathFromSource(enviro,enviroStart );
				double enviroVal = Double.MAX_VALUE;
				for(Node n:enviroCalc.getNodes())
					if(n.getName().equals(enviro.getEndNodeName()))
						enviroVal = n.getEnviro();
				//System.out.println(enviroVal);
				
				example= e.clone();
				double fitness = 0;
				
				LinkedList<RoadData> roads = new LinkedList<RoadData>();
				for (Node n: example.getNodes())
				{
					for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
					{
						RoadData road = roadPairs.getValue();
						roads.add(road);
						double weight = config.simpleCalcDistance(n, roadPairs.getKey(), distance.getCar());
						//System.out.println("weight"+weight);
						n.addDestination(roadPairs.getKey(), weight);
					}
					
				}
				
				example =  Dijkstra.calculateShortestPathFromSource(example, example.getNodeByName(example.getStartNodeName()));
				double efficiencyOfPath = example.getNodeByName(example.getEndNodeName()).getEfficency();
				//System.out.println("EfficiencyGoal:"+distanceVal);
				//System.out.println("efficiency: "+efficiencyOfPath);
				double enviroCostOfPath = example.getNodeByName(example.getEndNodeName()).getEnviro();
				//System.out.println("EnviroGoal: "+enviroVal);
				//System.out.println("enviro: "+enviroCostOfPath);
				
				double efficiencyDiff =distanceVal/efficiencyOfPath;
				double enviroDiff =  enviroVal/enviroCostOfPath;
				//System.out.println("EfficiencyDiff"+efficiencyDiff + " EnviroDiff:"+enviroDiff);
				
				fitness+= (1-config.enviromentFactor)*efficiencyDiff + config.enviromentFactor*enviroDiff;
				//System.out.println(fitness);
				client.send(String.valueOf(fitness));
				client.recieve();
			}
			else if(val.compareTo("getEn")==0)
			{
				//compare for enviroment
				Graph e=cases.get(inter%cases.size());
				inter+=1;
				example=  e.clone();
				Graph distance = example.clone();
				for (Node n: distance.getNodes())
				{
					for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
					{
						RoadData road = roadPairs.getValue();
						double weight = config.simpleCalcDistance(n, roadPairs.getKey(), distance.getCar());
						n.addDestination(roadPairs.getKey(), weight);
					}
					
				}
				Node distanceStart = distance.getNodeByName(distance.getStartNodeName());
				Graph distanceCalc = Dijkstra.calculateShortestPathFromSource(distance,distanceStart );
				double distanceVal = Double.MAX_VALUE;
				for(Node n:distanceCalc.getNodes())
					if(n.getName().equals(distance.getEndNodeName()))
						distanceVal = n.getEfficency();
				//System.out.println(distanceVal);
				
				Graph enviro = example.clone();
				for (Node n: enviro.getNodes())
				{
					for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
					{
						RoadData road = roadPairs.getValue();
						double weight = config.simpleCalcEnviro(n, roadPairs.getKey(), enviro.getCar());
						n.addDestination(roadPairs.getKey(), weight);
					}
					
				}
				Node enviroStart = enviro.getNodeByName(enviro.getStartNodeName());
				Graph enviroCalc = Dijkstra.calculateShortestPathFromSource(enviro,enviroStart );
				double enviroVal = Double.MAX_VALUE;
				for(Node n:enviroCalc.getNodes())
					if(n.getName().equals(enviro.getEndNodeName()))
						enviroVal = n.getEnviro();
				//System.out.println(enviroVal);
				
				example= e.clone();
				double fitness = 0;
				
				LinkedList<RoadData> roads = new LinkedList<RoadData>();
				for (Node n: example.getNodes())
				{
					for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
					{
						RoadData road = roadPairs.getValue();
						roads.add(road);
						double weight = config.simpleCalcEnviro(n, roadPairs.getKey(), distance.getCar());
						//System.out.println("weight"+weight);
						n.addDestination(roadPairs.getKey(), weight);
					}
					
				}
				
				example =  Dijkstra.calculateShortestPathFromSource(example, example.getNodeByName(example.getStartNodeName()));
				double efficiencyOfPath = example.getNodeByName(example.getEndNodeName()).getEfficency();
				//System.out.println("EfficiencyGoal:"+distanceVal);
				//System.out.println("efficiency: "+efficiencyOfPath);
				double enviroCostOfPath = example.getNodeByName(example.getEndNodeName()).getEnviro();
				//System.out.println("EnviroGoal: "+enviroVal);
				//System.out.println("enviro: "+enviroCostOfPath);
				
				double efficiencyDiff =distanceVal/efficiencyOfPath;
				double enviroDiff =  enviroVal/enviroCostOfPath;
				//System.out.println("EfficiencyDiff"+efficiencyDiff + " EnviroDiff:"+enviroDiff);
				
				fitness+= (1-config.enviromentFactor)*efficiencyDiff + config.enviromentFactor*enviroDiff;
				//System.out.println(fitness);
				client.send(String.valueOf(fitness));
				client.recieve();
			}
			else if(val.compareTo("Gen")==0)
			{
				//GenerateData
				client.send("Can I have n please");
				int Maxn =Integer.parseInt( client.recieve());
				for(int i=0;i<Maxn;i++ )
				{
					CarData car = cars.cars.get(r.nextInt(cars.cars.size()));
					Graph g = example.clone(car);
					g.setStartNodeName(nodeNames.get(r.nextInt(nodeNames.size())));
					String end = nodeNames.get(r.nextInt(nodeNames.size()));
					while (end.equals(g.getStartNodeName()))
					{
						end = nodeNames.get(r.nextInt(nodeNames.size()));
					}
					g.setEndNodeName(end);
					cases.add(g.clone(car));
					System.out.println(i+ ": Start: "+g.getStartNodeName()+", END: "+g.getEndNodeName());
				}
				
			}
			
				
			
		}
	}

}
