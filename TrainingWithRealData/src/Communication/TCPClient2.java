package Communication;


import java.io.*;
import java.net.*;
import java.util.Scanner;


public class TCPClient2
{
	public Socket socket;
	private Scanner scanner;
	private BufferedReader stdIn;
	public Boolean connected;
	
	public TCPClient2(InetAddress serverAddress,int serverPort)throws Exception{
		this.socket = new Socket(serverAddress, serverPort);
		this.scanner = new Scanner(System.in); 
		this.stdIn = new BufferedReader(new InputStreamReader(this.socket.getInputStream(),"UTF-8"));
		this.connected=true;
	}
	
	public void send(String message)throws IOException{
		String input = message;
		PrintWriter out = new PrintWriter(this.socket.getOutputStream(),true);
		out.println(input);
		out.flush();
		//System.out.println(message);
	}
	
	public String recieve()
	{
		String in = null;
		try {
			in = this.stdIn.readLine();
			//System.out.println(in);
			if (in.compareTo("END")==0)
			{
				this.socket.close();
				this.connected=false;
			}
			return in;
			
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return in;
		
	}
	

}