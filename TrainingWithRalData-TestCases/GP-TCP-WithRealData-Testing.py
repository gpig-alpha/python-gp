import random
import operator
import numpy
import pickle
from functools import partial
import itertools
from array import *
import matplotlib.pyplot as plt
from scipy.stats import mannwhitneyu

import TCPServer

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp


pset = gp.PrimitiveSetTyped("MAIN",[float, float, float, float, float, float, float, float, float], float )
#pset.addPrimitive(operator.and_, [bool, bool], bool)
#pset.addPrimitive(operator.or_, [bool, bool], bool)
#pset.addPrimitive(operator.not_, [bool], bool)

def protectedDiv(left, right):
    try: return left / right
    except ZeroDivisionError: return 1

pset.addPrimitive(operator.add, [float,float], float)
pset.addPrimitive(operator.sub, [float,float], float)
pset.addPrimitive(operator.mul, [float,float], float)
pset.addPrimitive(protectedDiv, [float,float], float)
pset.renameArguments(ARG0='priority')
pset.renameArguments(ARG1='maxSpeed')
pset.renameArguments(ARG2='pollutionGrade')
pset.renameArguments(ARG3='numberOfLanes')
pset.renameArguments(ARG4='distance')
pset.renameArguments(ARG5='speedLimit')
pset.renameArguments(ARG6='conjestion')
pset.renameArguments(ARG7='cleanFactor')
pset.renameArguments(ARG8='open')

def if_then_else(input, output1, output2):
    if input: return output1
    else: return output2

#pset.addPrimitive(operator.lt, [float, float], bool)
#pset.addPrimitive(operator.eq, [float, float], bool)
#pset.addPrimitive(if_then_else, [bool, float, float], float)

pset.addEphemeralConstant("rand100", lambda: random.random() * 100, float)
#pset.addTerminal(False, bool)
#pset.addTerminal(True, bool)

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=1, max_=3)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("compile", gp.compile, pset=pset)

def evalind(individual):
    func = toolbox.compile(expr=individual)
    result=0
    conn.send("getIn")
    val=""
    while True:
        val=conn.recieve()
        if val=="NOMORE":
            break;
        inputs = [float(x) for x in val.split(',') ]
        priority = inputs[0]
        maxSpeed = inputs[1]
        pollutionGrade = inputs[2]
        numberOfLanes = inputs[3]
        distance = inputs[4]
        speedLimit = inputs[5]
        conjestion = inputs[6]
        cleanFactor = inputs[7]
        open = inputs[8]
        conn.send(str(func(priority, maxSpeed, pollutionGrade, numberOfLanes,  distance, speedLimit, conjestion, cleanFactor, open)))
    conn.send("FitPls")
    result  += float(conn.recieve())
    conn.send("Recieved")
    return result, 
    
toolbox.register("evaluate", evalind)
toolbox.register("select", tools.selDoubleTournament, fitness_size=5,parsimony_size=1.3,fitness_first=True)
toolbox.register("mate", gp.cxOnePoint)
toolbox.register("expr_mut", gp.genHalfAndHalf, min_=0, max_=3)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

toolbox.decorate("mate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))
toolbox.decorate("mutate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))

def evaluate(func, priority, maxSpeed, pollutionGrade, numberOfLanes, distance, speedLimit, conjestion,  cleanFactor, open):
    return func(priority, maxSpeed, pollutionGrade, numberOfLanes, distance, speedLimit, conjestion, cleanFactor, open)

def evaluateIndiviudalNTimes(n, individual):
    result=array('f', [])
    for i in range(n):
        result.append(evalind(individual)[0])
    return result
    
def generateData(n):
    conn.send("Gen")
    conn.recieve()
    conn.send(str(n))
    
def evaluateEfficiency(n):
    result = array('f', [])
    for i in range(n):
        conn.send('getEf')
        val=conn.recieve()
        result.append(float(val))
        conn.send('Recieved')
    return result

def evaluateEnviro(n):
    result = array('f', [])
    for i in range(n):
        conn.send('getEn')
        val=conn.recieve()
        result.append(float(val))
        conn.send('Recieved')
    return result
    
def main():
    with open("Individual-TheSecond", "rb") as file:
        individual = pickle.load(file)
    tree=gp.PrimitiveTree(individual)
    print(str(tree))
    N = 50
    generateData(N)
    indivResults = evaluateIndiviudalNTimes(N, individual)
    EffResults = evaluateEfficiency(N);
    EnResults = evaluateEnviro(N);
    counter=0
    for i in indivResults:
        print(str(counter)+": "+str(i))
        counter+=1
    counter=0
    for i in EffResults:
        print(str(counter)+": "+str(i))
        counter+=1
    counter=0
    for i in EnResults:
        print(str(counter)+": "+str(i))
        counter+=1
    s, p = mannwhitneyu(indivResults, EffResults)
    print('individual compared to efficiency=%.3f,p=%.3f'%(s, p))
    s, p = mannwhitneyu(indivResults, EnResults)
    print('individual compared to envoroment=%.3f,p=%.3f'%(s, p))
    s, p = mannwhitneyu(EnResults, EffResults)
    print('enviroment compared to efficiency=%.3f,p=%.3f'%(s, p))
    
    data = [indivResults,EffResults, EnResults ]
    fig, ax = plt.subplots()
    ax.set_title("Comparing trained,efficiency and enviromental indiviudals")
    ax.boxplot(data)
    plt.show()
    
if __name__ == "__main__":
    conn=TCPServer.Connection()
    main()
    conn.end()
    
