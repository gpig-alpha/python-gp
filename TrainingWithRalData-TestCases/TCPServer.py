import socket


class Connection:
    def __init__(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(("localhost", 5000))
        server_socket.listen(5)
        self.client_socket, address = server_socket.accept()
        #print("I got a connection")
        
    def send (self, message):
        self.client_socket.send(bytes(message+"\n",'UTF-8'))
        #print(str(message).rstrip()+" Sent")
    
    def recieve(self):
        data = self.client_socket.recv(1024)
       # print(data.decode(encoding='UTF-8').rstrip())
        return data.decode(encoding='UTF-8').rstrip()
        
    def end(self):
        self.client_socket.send((bytes("END"+"\n",'UTF-8')))
        self.client_socket.close()
"""
conn = Connection()
conn.send("Hello World")
conn.send("Hello Worldd")
conn.send("UnExpected")
val=conn.recieve()

"""
   
