package SimpleRoadNetwok;

import java.util.*;
import java.util.Map.Entry;

public class Node {
	
	private String name;
	
	private List<Node> shortestPath = new LinkedList<Node>();
	
	private Double distance = Double.MAX_VALUE;
	
	private Double efficency = Double.MAX_VALUE;
	
	private Double enviro = Double.MAX_VALUE;


	Map<Node,Double> adjacentNodes = new HashMap<>();
	
	Map<Node,RoadData> roadDatas = new HashMap<>();
	

	public void addDestination(Node destination, double distance)
	{
		adjacentNodes.put(destination, distance);
	}
	
	public void addRoadData(Node destination,RoadData data)
	{
		roadDatas.put(destination,data);
	}
	
	public Node(String name)
	{
		this.name =name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Node> getShortestPath() {
		return shortestPath;
	}

	public void setShortestPath(List<Node> shortestPath) {
		this.shortestPath = shortestPath;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Map<Node, Double> getAdjacentNodes() {
		return adjacentNodes;
	}

	public void setAdjacentNodes(Map<Node, Double> adjacentNodes) {
		this.adjacentNodes = adjacentNodes;
	}
	
	public Map<Node, RoadData> getRoadDatas() {
		return roadDatas;
	}

	public void setRoadDatas(Map<Node, RoadData> roadDatas) {
		this.roadDatas = roadDatas;
	}
	
	
	public Double getEfficency() {
		return efficency;
	}

	public void setEfficency(Double efficency) {
		this.efficency = efficency;
	}
	
	
	
	public Double getEnviro() {
		return enviro;
	}

	public void setEnviro(Double enviro) {
		this.enviro = enviro;
	}

	public Node clone()
	{
		Node value = new Node(this.name);
		value.setAdjacentNodes(this.getAdjacentNodes());
		value.setRoadDatas(this.getRoadDatas());
		return value;
		
	}


	@Override
	public String toString() {
		return "Node [name=" + name + " distance="+ distance+"]";
	}
	
	

}
