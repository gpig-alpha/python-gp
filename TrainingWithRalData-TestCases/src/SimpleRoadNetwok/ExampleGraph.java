package SimpleRoadNetwok;

import java.util.Map.Entry;
import java.util.Random;

public class ExampleGraph extends Graph{
	
	
	Node nodeA = new Node("START");
	Node nodeB = new Node("B");
	Node nodeC = new Node("C");
	Node nodeD = new Node("D"); 
	Node nodeE = new Node("E");
	Node nodeF = new Node("F");
	Node nodeG = new Node("G");
	Node nodeH = new Node("H");
	Node nodeI = new Node("END");
	
	
	
	
	


	public ExampleGraph(CarData car,int option) {
		super(car);
		
		// 0 no option selected must calculate own weights
		// 1 uses effeciency to calculate weights 
		// 2 uses enviromental impact to calculate weights

		/*
		// Realistically should come from a file and be parsed in 
		//numberoflanes,distance,speedLimit,conjestion,cleanfactor,open
		nodeA.addRoadData(nodeB, new RoadData(1,100,30,20,30,true));
		nodeB.addRoadData(nodeC, new RoadData(1,64 ,30,30,40,true));
		nodeA.addRoadData(nodeD, new RoadData(2,100,40,20,20,true));
		nodeB.addRoadData(nodeE, new RoadData(1,35 ,30,30,80,true));
		nodeC.addRoadData(nodeF, new RoadData(1,30 ,30,50,30,true));
		nodeD.addRoadData(nodeE, new RoadData(1,65 ,30,10,80,true));
		nodeE.addRoadData(nodeF, new RoadData(1,60 ,30,20,40,true));
		nodeD.addRoadData(nodeG, new RoadData(2,40 ,300,80,20,true));
		nodeE.addRoadData(nodeH, new RoadData(1,60 ,30,20,50,true));
		nodeF.addRoadData(nodeI, new RoadData(1,150,30,10,10,true));
		nodeG.addRoadData(nodeH, new RoadData(2,80 ,300,20,10,true));
		nodeH.addRoadData(nodeI, new RoadData(2,120,40,30,10,true));
		*/
		if(option!=-1) {
		Random r = new Random();
		nodeA.addRoadData(nodeB, new RoadData(1+r.nextInt(2),20+r.nextInt(199),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeB.addRoadData(nodeC, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeA.addRoadData(nodeD, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeB.addRoadData(nodeE, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeC.addRoadData(nodeF, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeD.addRoadData(nodeE, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeE.addRoadData(nodeF, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeD.addRoadData(nodeG, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeE.addRoadData(nodeH, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeF.addRoadData(nodeI, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeG.addRoadData(nodeH, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		nodeH.addRoadData(nodeI, new RoadData(1+r.nextInt(2),20+r.nextInt(200),20+r.nextInt(50),r.nextInt(100),r.nextInt(100),r.nextFloat()<0.02?false:true));
		
		}
		
		if(option==1) {
		nodeA.addDestination(nodeB, simpleCalcDistance(nodeA,nodeB,car));
		nodeB.addDestination(nodeC, simpleCalcDistance(nodeB,nodeC,car));
		nodeA.addDestination(nodeD, simpleCalcDistance(nodeA,nodeD,car));
		nodeB.addDestination(nodeE, simpleCalcDistance(nodeB,nodeE,car));
		nodeC.addDestination(nodeF, simpleCalcDistance(nodeC,nodeF,car));
		nodeD.addDestination(nodeE, simpleCalcDistance(nodeD,nodeE,car));
		nodeE.addDestination(nodeF, simpleCalcDistance(nodeE,nodeF,car));
		nodeD.addDestination(nodeG, simpleCalcDistance(nodeD,nodeG,car));
		nodeE.addDestination(nodeH, simpleCalcDistance(nodeE,nodeH,car));
		nodeF.addDestination(nodeI, simpleCalcDistance(nodeF,nodeI,car));
		nodeG.addDestination(nodeH, simpleCalcDistance(nodeG,nodeH,car));
		nodeH.addDestination(nodeI, simpleCalcDistance(nodeH,nodeI,car));

		}
		else if (option==2)
		{
		nodeA.addDestination(nodeB, simpleCalcEnviro(nodeA,nodeB,car));
		nodeB.addDestination(nodeC, simpleCalcEnviro(nodeB,nodeC,car));
		nodeA.addDestination(nodeD, simpleCalcEnviro(nodeA,nodeD,car));
		nodeB.addDestination(nodeE, simpleCalcEnviro(nodeB,nodeE,car));
		nodeC.addDestination(nodeF, simpleCalcEnviro(nodeC,nodeF,car));
		nodeD.addDestination(nodeE, simpleCalcEnviro(nodeD,nodeE,car));
		nodeE.addDestination(nodeF, simpleCalcEnviro(nodeE,nodeF,car));
		nodeD.addDestination(nodeG, simpleCalcEnviro(nodeD,nodeG,car));
		nodeE.addDestination(nodeH, simpleCalcEnviro(nodeE,nodeH,car));
		nodeF.addDestination(nodeI, simpleCalcEnviro(nodeF,nodeI,car));
		nodeG.addDestination(nodeH, simpleCalcEnviro(nodeG,nodeH,car));
		nodeH.addDestination(nodeI, simpleCalcEnviro(nodeH,nodeI,car));
		}

		 
		 
		this.addNode(nodeA);
		this.addNode(nodeB);
		this.addNode(nodeC);
		this.addNode(nodeD);
		this.addNode(nodeE);
		this.addNode(nodeF);
		this.addNode(nodeG);
		this.addNode(nodeH);
		this.addNode(nodeI);
	}







	@Override
	public ExampleGraph clone() {
		
			//should clone the car but currently not using this so hey ho
			ExampleGraph value = new ExampleGraph(getCar(),-1);
			for(Node n : super.nodes)
			{
				Node nodeToAdd = new Node(n.getName());
				
				value.addNode(nodeToAdd);
			}
			for(Node n: nodes) {
				
				for (Node x: value.nodes)
				{
					if (n.getName().equals(x.getName()))
					for (Node y:value.nodes)
					{
						for (Entry<Node, RoadData> adjacencyPair : n.getRoadDatas().entrySet())
						{
							if(y.getName().equals(adjacencyPair.getKey().getName()))
								x.addRoadData(y, adjacencyPair.getValue().clone());
						}
						for (Entry<Node, Double> adjacencyPair : n.getAdjacentNodes().entrySet())
						{
							if(y.getName().equals(adjacencyPair.getKey().getName()))
								x.addDestination(y, adjacencyPair.getValue());
						}
					}
				}
			}
			return value;
					
		
	}
	
	
	

}
