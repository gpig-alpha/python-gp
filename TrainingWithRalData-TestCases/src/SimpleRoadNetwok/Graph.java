package SimpleRoadNetwok;
import java.util.*;
import java.util.Map.Entry;

public class Graph {

	protected Set<Node> nodes = new HashSet<>();
	private CarData car;
	private String startNodeName;
	private String endNodeName;
	
	
	public static double simpleCalcDistance(Node source,Node target,CarData car)
	{
		double value = Double.MAX_VALUE;
		for (Entry<Node,RoadData> road:source.getRoadDatas().entrySet())
		{
			if (road.getKey().getName().equals(target.getName()))
			{
				
				value =  ((double)road.getValue().getDistance()/(Math.min(road.getValue().getSpeedLimit(),car.getMaxSpeed())) + 0.1*(road.getValue().getDistance()*road.getValue().getConjestion()/road.getValue().getNumberOfLanes()) + 0.3 * road.getValue().getDistance()* (car.getMaxSpeed()<road.getValue().getSpeedLimit()? 1 : 0));
				
				if (!road.getValue().isOpen())
					value=(int) Double.MAX_VALUE/2;

			}
		}
		return value;
	}
	
	public static double simpleCalcEnviro(Node source,Node target,CarData car)
	{
		double value = Double.MAX_VALUE/2f;
		for (Entry<Node,RoadData> road:source.getRoadDatas().entrySet())
		{
			if (road.getKey().getName().equals(target.getName()))
			{
				value =  (Math.max((road.getValue().getConjestion()/10)*5 + car.getPollutionGrade()*10 + road.getValue().getCleanFactor() - 50 ,1));
				return value;
			}
		}
		System.err.println("Not connected");
		return value;
	}
	
	public Node getNodeByName(String name)
	{
		Node ret=null;
		for(Node n:this.getNodes())
		{
			if (n.getName().equals(name))
			{
				ret = n;
				return ret;
			}
		}
		System.err.println("No node by that name: "+name);
		return ret;
	}
	//public void AssignNodeDestination(Node start,Node end, int distance )
	//{
	//	start.addDestination(end, distance);
	//}
	
	public CarData getCar() {
		return car;
	}

	public void setCar(CarData car) {
		this.car = car;
	}

	public Graph(CarData car)
	{
		this.car=car;
	}
	
	public void addNode(Node nodeA)
	{
		nodes.add(nodeA);
	}
	
	public Set<Node> getNodes() {
		return nodes;
	}

	public void setNodes(Set<Node> nodes) {
		this.nodes = nodes;
	}
	
	
	
	public String getStartNodeName() {
		return startNodeName;
	}

	public void setStartNodeName(String startNodeName) {
		this.startNodeName = startNodeName;
	}

	public String getEndNodeName() {
		return endNodeName;
	}

	public void setEndNodeName(String endNodeName) {
		this.endNodeName = endNodeName;
	}

	public Graph clone ()
	{
		//should clone the car but currently not using this so hey ho
		Graph value = new Graph(this.car);
		value.setStartNodeName(this.getStartNodeName());
		value.setEndNodeName(this.getEndNodeName());
		for(Node n : nodes)
		{
			Node nodeToAdd = new Node(n.getName());
			
			value.addNode(nodeToAdd);
		}
		for(Node n: nodes) {
			
			for (Node x: value.nodes)
			{
				if (n.getName().equals(x.getName()))
				for (Node y:value.nodes)
				{
					for (Entry<Node, RoadData> adjacencyPair : n.getRoadDatas().entrySet())
					{
						if(y.getName().equals(adjacencyPair.getKey().getName()))
							x.addRoadData(y, adjacencyPair.getValue().clone());
					}
					for (Entry<Node, Double> adjacencyPair : n.getAdjacentNodes().entrySet())
					{
						if(y.getName().equals(adjacencyPair.getKey().getName()))
							x.addDestination(y, adjacencyPair.getValue());
					}
				}
			}
		}
		return value;
				
	}
	
	public Graph clone (CarData car)
	{
		//should clone the car but currently not using this so hey ho
		Graph value = new Graph(car);
		value.setStartNodeName(this.getStartNodeName());
		value.setEndNodeName(this.getEndNodeName());
		for(Node n : nodes)
		{
			Node nodeToAdd = new Node(n.getName());
			
			value.addNode(nodeToAdd);
		}
		for(Node n: nodes) {
			
			for (Node x: value.nodes)
			{
				if (n.getName().equals(x.getName()))
				for (Node y:value.nodes)
				{
					for (Entry<Node, RoadData> adjacencyPair : n.getRoadDatas().entrySet())
					{
						if(y.getName().equals(adjacencyPair.getKey().getName()))
							x.addRoadData(y, adjacencyPair.getValue().clone());
					}
					for (Entry<Node, Double> adjacencyPair : n.getAdjacentNodes().entrySet())
					{
						if(y.getName().equals(adjacencyPair.getKey().getName()))
							x.addDestination(y, adjacencyPair.getValue());
					}
				}
			}
		}
		return value;
				
	}

}
