package SimpleRoadNetwok;

import java.util.*;
import java.util.Map.Entry;

import Configuration.config;

public class Dijkstra {
	
	public static Graph calculateShortestPathFromSource(Graph graph, Node source)
	{
		source.setDistance((double) 0);
		source.setEnviro(0d);
		source.setEfficency(0d);
		
		Set<Node> settledNodes = new HashSet<>();
		Set<Node> unsettledNodes = new HashSet<>();
		unsettledNodes.add(source);
		
		while (unsettledNodes.size()!=0)
		{
			Node currentNode = getLowestDistanceNode(unsettledNodes);
			unsettledNodes.remove(currentNode);
			if(currentNode!=null)
			for (Entry<Node, Double> adjacencyPair : currentNode.getAdjacentNodes().entrySet()) {
				Node adjacentNode = adjacencyPair.getKey();
				Double edgeWeigh = adjacencyPair.getValue();
				
				if(!settledNodes.contains(adjacentNode))
				{
					CalculateMinimumDistance(adjacentNode,edgeWeigh,currentNode,graph);
					unsettledNodes.add(adjacentNode);
				}
			}
			settledNodes.add(currentNode);
		}
		return graph;
	}
	
	private static void CalculateMinimumDistance(Node evaluationNode,Double edgeWeigh,Node sourceNode, Graph graph) {
		Double sourceEfficiency = sourceNode.getEfficency();
		Double sourceDistance = sourceNode.getDistance();
		Double sourceEnviro = sourceNode.getEnviro();
		if (sourceDistance + edgeWeigh<evaluationNode.getDistance())
		{
			evaluationNode.setDistance(sourceDistance+edgeWeigh);
			evaluationNode.setEfficency(sourceEfficiency+getEfficiency(sourceNode,evaluationNode,graph.getCar()));
			evaluationNode.setEnviro(sourceEnviro + getEnviro(sourceNode,evaluationNode,graph.getCar()));
			LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
			shortestPath.add(sourceNode);
			evaluationNode.setShortestPath(shortestPath);
		}
	}
	
	private static double getEfficiency(Node sourceNode, Node targetNode,CarData car)
	{
		return config.simpleCalcDistance(sourceNode, targetNode, car);
	}
	
	private static double getEnviro(Node sourceNode,Node targetNode,CarData car)
	{
		return config.simpleCalcEnviro(sourceNode, targetNode, car);
	}
	
	private static Node getLowestDistanceNode(Set<Node> unsettledNodes) {
		Node lowestDistanceNode = null;
		double lowestDistance = Double.MAX_VALUE;
		for (Node node:unsettledNodes)
		{
			double nodeDistance = node.getDistance();
			if(nodeDistance<lowestDistance)
			{
				lowestDistance = nodeDistance;
				lowestDistanceNode=node;
			}
		}
		if (lowestDistanceNode==null)
			return unsettledNodes.iterator().next();
		return lowestDistanceNode;
	}
	

}
