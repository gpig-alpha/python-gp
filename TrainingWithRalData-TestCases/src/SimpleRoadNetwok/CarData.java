package SimpleRoadNetwok;

public class CarData {
	private String name;
	private int priority;
	private int maxSpeed;
	private int pollutionGrade;
	
	public CarData(String name)
	{
		this.name=name;
	}
	
	public CarData(String name,int priority, int maxSpeed,int pollutionGrade)
	{
		this.name=name;
		this.priority=priority;
		this.maxSpeed=maxSpeed;
		this.pollutionGrade=pollutionGrade;
	}

	public String getName() {
		return name;
	}


	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public int getPollutionGrade() {
		return pollutionGrade;
	}

	public void setPollutionGrade(int pollutionGrade) {
		this.pollutionGrade = pollutionGrade;
	}

	@Override
	public String toString() {
		return priority + "," + maxSpeed + ","
				+ pollutionGrade;
	}
	

}
