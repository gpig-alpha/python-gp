package Communication;


import java.io.*;
import java.net.*;
import java.util.*;
import java.util.Map.Entry;

import SimpleRoadNetwok.*;
import DBReader.*;
import Configuration.*;

import java.math.*;

public class Start {


	public static void main(String[] args) throws Exception{
		TCPClient2 client = new TCPClient2(InetAddress.getByName("localhost"),5000);
		System.out.println("\r\n Connected to Server: "+client.socket.getInetAddress());
		Random r = new Random();
		LinkedList<GPData> d = new LinkedList<GPData>() ;
		DB db = new DB();
		Graph example= db.GenerateGraph(new CarData("Shouldn't be used",0,0,0));
		LinkedList<Graph> cases = new LinkedList<Graph>();
	/*
		LinkedList<String> starts = new LinkedList<String>();
		starts.add("3632304590");
		starts.add("2156857867");
		starts.add("262974126");
		starts.add("259178830");
		starts.add("30477795");
		starts.add("259178817");
		starts.add("262974072");
		starts.add("262974081");
		starts.add("259178871");
		starts.add("30477822");
		starts.add("259178688");
		starts.add("4285740156");
		LinkedList<String> ends = new LinkedList<String>();
		ends.add("257923774");
		ends.add("262974121");
		ends.add("259178704");
		ends.add("2156812564");
		ends.add("259178633");
		ends.add("262974112");
		ends.add("259031774");
		ends.add("1258574712");
		ends.add("259178857");
		ends.add("259031777");
		ends.add("1606080232");
		ends.add("262974100");
		*/
		LinkedList<String> starts = new LinkedList<String>();
		starts.add("262974187");
		starts.add("1644324906");
		starts.add("27231337");
		starts.add("289939207");
		starts.add("27406183");
		starts.add("27406183");
		starts.add("1843446904");
		starts.add("1605560959");
		starts.add("1258574691");
		starts.add("6290198624");
		starts.add("2573333475");
		starts.add("2368857956");
		LinkedList<String> ends = new LinkedList<String>();
		ends.add("259031681");
		ends.add("12717063");
		ends.add("259178392");
		ends.add("259030188");
		ends.add("457696763");
		ends.add("262974105");
		ends.add("283443926");
		ends.add("2367655424");
		ends.add("283443988");
		ends.add("30477789");
		ends.add("262978158");
		ends.add("259178688");
		
		int pointer = r.nextInt(ends.size());
		ExampleCarDatas cars= new ExampleCarDatas();
		for(CarData car:cars.cars )
		{
			Graph g = db.GenerateGraph(car);
			g.setStartNodeName(starts.get(pointer%starts.size()));
			g.setEndNodeName(ends.get(pointer%ends.size()));
			cases.add(g.clone(car));
			pointer+=1;
			
		}
		
		while (client.connected)
		{
			
			

			String val=client.recieve();
			if(val.compareTo("GEN")==0)
			{
				
				cases = new LinkedList<Graph>();
				pointer = r.nextInt(ends.size());
		
				for(CarData car:cars.cars )
				{
					Graph g = db.GenerateGraph(car);
					g.setStartNodeName(starts.get(pointer%starts.size()));
					g.setEndNodeName(ends.get(pointer%ends.size()));
					cases.add(g.clone(car));
					pointer+=1;
					
				}
				client.send("Done");
				client.recieve();
			}
			
			if (val.compareTo("getIn")==0)
			{
				client.send(String.valueOf(cars.cars.size()));
				client.recieve();
				for(Graph e : cases)
				{
					
					example=  e.clone();
					Graph distance = example.clone();
					for (Node n: distance.getNodes())
					{
						for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
						{
							RoadData road = roadPairs.getValue();
							double weight = config.simpleCalcDistance(n, roadPairs.getKey(), distance.getCar());
							n.addDestination(roadPairs.getKey(), weight);
						}
						
					}
					Node distanceStart = distance.getNodeByName(distance.getStartNodeName());
					Graph distanceCalc = Dijkstra.calculateShortestPathFromSource(distance,distanceStart );
					double distanceVal = Double.MAX_VALUE;
					for(Node n:distanceCalc.getNodes())
						if(n.getName().equals(distance.getEndNodeName()))
							distanceVal = n.getEfficency();
					//System.out.println(distanceVal);
					
					Graph enviro = example.clone();
					for (Node n: enviro.getNodes())
					{
						for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
						{
							RoadData road = roadPairs.getValue();
							double weight = config.simpleCalcEnviro(n, roadPairs.getKey(), enviro.getCar());
							n.addDestination(roadPairs.getKey(), weight);
						}
						
					}
					Node enviroStart = enviro.getNodeByName(enviro.getStartNodeName());
					Graph enviroCalc = Dijkstra.calculateShortestPathFromSource(enviro,enviroStart );
					double enviroVal = Double.MAX_VALUE;
					for(Node n:enviroCalc.getNodes())
						if(n.getName().equals(enviro.getEndNodeName()))
							enviroVal = n.getEnviro();
					//System.out.println(enviroVal);
					
					example= e.clone();
					double fitness = 0;
					
					LinkedList<RoadData> roads = new LinkedList<RoadData>();
					for (Node n: example.getNodes())
					{
						for (Entry<Node, RoadData> roadPairs:n.getRoadDatas().entrySet())
						{
							RoadData road = roadPairs.getValue();
							roads.add(road);
							//d.add(new GPData(e.getCar(),road));
							String message = String.valueOf(e.getCar().toString())+","+String.valueOf(road.toString());
							client.send(message);
							double weight = Float.parseFloat(client.recieve());
							//System.out.println("weight"+weight);
							n.addDestination(roadPairs.getKey(), weight);
						}
						
					}
					client.send("NOMORE");	
					example =  Dijkstra.calculateShortestPathFromSource(example, example.getNodeByName(example.getStartNodeName()));
					double efficiencyOfPath = example.getNodeByName(example.getEndNodeName()).getEfficency();
					//System.out.println("EfficiencyGoal:"+distanceVal);
					//System.out.println("efficiency: "+efficiencyOfPath);
					double enviroCostOfPath = example.getNodeByName(example.getEndNodeName()).getEnviro();
					//System.out.println("EnviroGoal: "+enviroVal);
					//System.out.println("enviro: "+enviroCostOfPath);
					
					double efficiencyDiff =distanceVal/efficiencyOfPath;
					double enviroDiff =  enviroVal/enviroCostOfPath;
					//System.out.println("EfficiencyDiff"+efficiencyDiff + " EnviroDiff:"+enviroDiff);
					
					fitness+= (1-config.enviromentFactor)*efficiencyDiff + config.enviromentFactor*enviroDiff;
					//System.out.println(fitness);
					client.recieve();
					client.send(String.valueOf(fitness));
					client.recieve();
				}
			}
			
				
			
		}
	}

}
