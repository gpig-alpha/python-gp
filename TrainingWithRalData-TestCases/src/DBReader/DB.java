package DBReader;

import SimpleRoadNetwok.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.*;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.json.simple.*;
import org.json.simple.parser.*; 





public class DB {
	public HttpResponse<String> response = null;
	private String data = null;
	private Object obj = null;
	private JSONArray array =null;
	
	public List<Node> nodes = new ArrayList<Node>();
	public List<Road> roadDatas = new ArrayList<Road>();
	private Random r = new Random();
	private float largeBuildingDensity=1;
	
	public DB() throws InterruptedException
	{
		

		URI url;
		try {
			url = new URI("http://104.197.194.54:5000/access_ways");
			HttpClient client = HttpClient.newHttpClient();
			HttpRequest request = HttpRequest.newBuilder().uri(url).header("Content-type", "json").build();
			try {
				response = client.send(request, BodyHandlers.ofString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		data = response.body();
		System.out.println(response.body());

	/*	
		try {
		File myObj = new File("RawData.txt");
		Scanner myReader = new Scanner(myObj);
		while (myReader.hasNextLine())
		{
			String d = myReader.nextLine();
			System.out.println(d);
			if( data!=null)
				data += d+"\n";
			else data=d+"\n";
		}
		myReader.close();
		*/
		obj = JSONValue.parse(data);
		array = (JSONArray)obj;
		//gets a row
		//System.out.println(array.get(1));
		
		//gets a cell in the row
		Object obj2 = JSONValue.parse(array.get(1).toString());
		JSONArray arrayInner = (JSONArray)obj2;
		//System.out.println(Integer.parseInt(arrayInner.get(1).toString())+Integer.parseInt(arrayInner.get(2).toString()));
	
		
		Graph g = GenerateGraph(new CarData("A",10,10,10));
		
		
		/*
		
		 }
		catch(FileNotFoundException e)
		{
			System.out.println("Error Occured");
			e.printStackTrace();
		} 
		*/
		

	}
	private void GenerateData()
	{
		roadDatas = new ArrayList<Road>();
		for (int i=0;i<array.size();i++)
		{
			roadDatas.add(GenerateRoad(i));
		}
	}
	
	private Road GenerateRoad(int index)
	{
		Object line = JSONValue.parse(array.get(index).toString());
		JSONArray arrayInner = (JSONArray) line;
		
		String highWay = arrayInner.get(11).toString();
		int highWayValue=getHighWayValue(highWay);
		
		String sideWalk = arrayInner.get(12).toString();
		int sideWalkValue=getSideWalkValue(sideWalk);
		
		float buildingDensity = 10*Float.parseFloat(arrayInner.get(7).toString())/largeBuildingDensity;
		//					Start Node						END Node									speed limit												road length									speedlimit										congestion									envirofactor			open
		return new Road(arrayInner.get(2).toString(), arrayInner.get(4).toString(), new RoadData(Integer.parseInt(arrayInner.get(9).toString()),Float.parseFloat(arrayInner.get(8).toString()),Integer.parseInt(arrayInner.get(10).toString()),(10-highWayValue)*r.nextFloat()*10,buildingDensity*(highWayValue+sideWalkValue),true));
	}
	
	private float getLargestBuildingDensity() 
	{
		float max = 0;
		for(int index=0;index<array.size();index++) {
			Object line = JSONValue.parse(array.get(index).toString());
			JSONArray arrayInner = (JSONArray) line;
			if(Float.parseFloat(arrayInner.get(7).toString())>max)
				max=Float.parseFloat(arrayInner.get(7).toString());
		
		}
		return max;
	}
	
	private void GenerateNodes()
	{
		nodes = new ArrayList<Node>();
		List<String> seen = new ArrayList<String>();
		
		for(Road r:roadDatas)
		{
			if (!seen.contains(r.getStartNodeID()))
			{
				nodes.add(new Node(r.getStartNodeID()));
				seen.add(r.getStartNodeID());
			}
			if (!seen.contains(r.getEndNodeID()))
			{
				nodes.add(new Node(r.getEndNodeID()));
				seen.add(r.getEndNodeID());
			}
		}
	}
	
	private int getHighWayValue(String highWay)
	{
		int highWayValue = 0;
		switch (highWay)
		{
		case "residential":
			highWayValue=8;
			break;
		case "living_street":
			highWayValue=6;
			break;
		case "tertiary":
			highWayValue=4;
			break;
		case "primary_link":
			highWayValue=2;
			break;
		case "primary":
			highWayValue=1;
			break;
		case "unclassified":
			highWayValue=4;
			break;
			
		}
		return highWayValue;
	
	}
	
	private int getSideWalkValue(String sideWalk)
	{
		int sideWalkValue =0;
		switch(sideWalk)
		{
		case "left":
			sideWalkValue=1;
			break;
		case "right":
			sideWalkValue=1;
			break;
		case "both":
			sideWalkValue=2;
			break;
		}
		
		return sideWalkValue;
	}
	
	private void ConnectNodes()
	{
		for(Road r:roadDatas)
		{
			getNodeByName(r.getStartNodeID()).addRoadData(getNodeByName(r.getEndNodeID()), r.getMyData());
			//getNodeByName(r.getEndNodeID()).addRoadData(getNodeByName(r.getStartNodeID()), r.getMyData());

		}
	}
	
	private Node getNodeByName(String name)
	{
		for (Node n:nodes)
		{
			if(n.getName().equals(name))
				return n;
		}
		return null;
	}
	
	private void renameNode(String name, String newName)
	{
		getNodeByName(name).setName(newName);
	}
	
	public List<String> GetLatLonOfNode(String name)
	{
		List<String> latLon = new ArrayList<String>();
		for(int index=0;index<array.size();index++) {
			Object line = JSONValue.parse(array.get(index).toString());
			JSONArray arrayInner = (JSONArray) line;
			if (arrayInner.get(2).toString().equals(name))
			{
				Object coordPair = JSONValue.parse(arrayInner.get(3).toString());
				JSONArray pair = (JSONArray) coordPair;
				latLon.add(pair.get(0).toString());
				latLon.add(pair.get(1).toString());
				break;
			}
			
		}
		
		return latLon;
	}
	
	public Graph GenerateGraph(CarData car)
	{
		largeBuildingDensity=getLargestBuildingDensity();
		GenerateData();
		GenerateNodes();
		ConnectNodes();
		Graph ret = new Graph(car);
		for(Node n:nodes)
		{
			ret.addNode(n);
		}
		
		return ret;
	}

}
