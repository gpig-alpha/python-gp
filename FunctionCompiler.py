import random
import operator
import numpy
import pickle
from functools import partial
import itertools
import sys

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

pset = gp.PrimitiveSetTyped("MAIN",[float, float, float, float, float, float, float, float, float], float )
#pset.addPrimitive(operator.and_, [bool, bool], bool)
#pset.addPrimitive(operator.or_, [bool, bool], bool)
#pset.addPrimitive(operator.not_, [bool], bool)

def protectedDiv(left, right):
    try: return left / right
    except ZeroDivisionError: return 1

pset.addPrimitive(operator.add, [float,float], float)
pset.addPrimitive(operator.sub, [float,float], float)
pset.addPrimitive(operator.mul, [float,float], float)
pset.addPrimitive(protectedDiv, [float,float], float)
pset.renameArguments(ARG0='priority')
pset.renameArguments(ARG1='maxSpeed')
pset.renameArguments(ARG2='pollutionGrade')
pset.renameArguments(ARG3='numberOfLanes')
pset.renameArguments(ARG4='distance')
pset.renameArguments(ARG5='speedLimit')
pset.renameArguments(ARG6='congestion')
pset.renameArguments(ARG7='cleanFactor')
pset.renameArguments(ARG8='open')

def if_then_else(input, output1, output2):
    if input: return output1
    else: return output2

#pset.addPrimitive(operator.lt, [float, float], bool)
#pset.addPrimitive(operator.eq, [float, float], bool)
#pset.addPrimitive(if_then_else, [bool, float, float], float)

pset.addEphemeralConstant("rand100", lambda: random.random() * 100, float)
#pset.addTerminal(False, bool)
#pset.addTerminal(True, bool)

creator.create("FitnessMax", base.Fitness, weights=(-1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=1, max_=3)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("compile", gp.compile, pset=pset)

def evalind(individual):
  return True
    
toolbox.register("evaluate", evalind)
toolbox.register("select", tools.selDoubleTournament, fitness_size=5,parsimony_size=1.3,fitness_first=True)
toolbox.register("mate", gp.cxOnePoint)
toolbox.register("expr_mut", gp.genHalfAndHalf, min_=0, max_=3)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

toolbox.decorate("mate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))
toolbox.decorate("mutate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))

def evaluate(priority, maxSpeed, pollutionGrade, numberOfLanes, distance, speedLimit, congestion,  cleanFactor, open):
    return func(priority, maxSpeed, pollutionGrade, numberOfLanes, distance, speedLimit, congestion, cleanFactor, open)


    

if __name__ == "__main__":
    with open("Individual", "rb") as file:
        individual = pickle.load(file)
    func = toolbox.compile(expr=individual)
    reading=True
    while reading:
        val= input("Input comma seperated line here: ")
        if val== "STOP":
            reading=False
            break;
        inputs = [float(x) for x in val.split(',') if x.strip().isdigit()]
        print(evaluate(inputs[0], inputs[1],inputs[2], inputs[3], inputs[4],inputs[5], inputs[6], inputs[7],inputs[8]))

"""
Evaluation variable definitions:
Priority: 1-100, decided by council
MaxSpeed : max speed of vehicle, in mph
PollutionGrade: 1-10, 10 being the worst, decided by some car people
Number of lanes: obtained from an OSM way
Distance: obtained from an OSM way's road_length
Speed limit: obtained from an OSM way's maxspeed
Congestion: value taken from OSM way's highway value and randomised
CleanFactor: determined from OSM way's highway, sidewalk value and building density
Open: is set to True if road is open, False if closed
"""

