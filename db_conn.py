import psycopg2
import pandas
import time

try :
    data = None
    con = psycopg2.connect(
    host = "104.197.194.54",
    port = "5431",
    database = "map",
    user = "postgres",
    password = "postgres")
    time.sleep(1)
    data = pandas.read_sql_query("SELECT * FROM ways", con)
except:
    print("Could not connect to database.")


if not data.empty:
    print(data)

