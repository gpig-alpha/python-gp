import random
import operator
import numpy
import pickle
from functools import partial
import itertools

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

pset = gp.PrimitiveSetTyped("MAIN",[float, float, float], float, "IN" )
#pset.addPrimitive(operator.and_, [bool, bool], bool)
#pset.addPrimitive(operator.or_, [bool, bool], bool)
#pset.addPrimitive(operator.not_, [bool], bool)

def protectedDiv(left, right):
    try: return left / right
    except ZeroDivisionError: return 1

pset.addPrimitive(operator.add, [float,float], float)
pset.addPrimitive(operator.sub, [float,float], float)
pset.addPrimitive(operator.mul, [float,float], float)
pset.addPrimitive(protectedDiv, [float,float], float)

def if_then_else(input, output1, output2):
    if input: return output1
    else: return output2

#pset.addPrimitive(operator.lt, [float, float], bool)
#pset.addPrimitive(operator.eq, [float, float], bool)
#pset.addPrimitive(if_then_else, [bool, float, float], float)

#pset.addEphemeralConstant("rand100", lambda: random.random() * 100, float)
#pset.addTerminal(False, bool)
#pset.addTerminal(True, bool)

creator.create("FitnessMax", base.Fitness, weights=(-1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=1, max_=2)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("compile", gp.compile, pset=pset)

def evalind(individual):
    func = toolbox.compile(expr=individual)
    result=0
    for i in range(0, 10):
        x = random.randrange(10)
        y = random.randrange(10)
        z = random.randrange(1, 10)
        
        result  += abs(func(x, y, z)-((x*x)/z + y*x))
    
    
    return result, 
    
toolbox.register("evaluate", evalind)
toolbox.register("select", tools.selDoubleTournament, fitness_size=5,parsimony_size=1.5,fitness_first=True)
toolbox.register("mate", gp.cxOnePoint)
toolbox.register("expr_mut", gp.genHalfAndHalf, min_=0, max_=2)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

toolbox.decorate("mate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))
toolbox.decorate("mutate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))


def main():
    random.seed()
    size=200
    NGEN=2000
    CXPB=0.5
    MUTPB=0.1
    pop=toolbox.population(n=size)
    start_gen=0
    hof=tools.HallOfFame(1)
    logb=tools.Logbook()


    stats_fit=tools.Statistics(lambda ind:ind.fitness.values)
    stats_size=tools.Statistics(key=operator.attrgetter("height"))
    stats=tools.MultiStatistics(fitness=stats_fit,size=stats_size)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)
    logb.header="gen","evals","fitness","size"
    logb.chapters["fitness"].header="avg","std","min","max"
    logb.chapters["size"].header="avg","std","min","max"
    
    algorithms.eaSimple(pop, toolbox, CXPB, MUTPB, NGEN, stats, halloffame=hof)
    """
    for gen in range(start_gen,NGEN+1):
        pop = toolbox.select(pop, len(pop))
        offspring=algorithms.varAnd(pop,toolbox,cxpb=CXPB,mutpb=MUTPB)#changr to offspring
        invalid_ind=[ind for ind in offspring if not ind.fitness.valid]
        fitnesses=toolbox.map(toolbox.evaluate,invalid_ind)
        for ind, fit in zip(invalid_ind,fitnesses):
            ind.fitness.values=fit
        pop=offspring
        hof.update(pop)
        record=stats.compile(pop)
        
        logb.record(gen=gen,evals=len(invalid_ind),**record)
        print(logb.stream)
        """
        
    tree=gp.PrimitiveTree(hof[0])
    print(str(tree))

    return pop, stats, hof

if __name__ == "__main__":
    main()
    
