import random
import operator
import numpy
import pickle
from functools import partial
import itertools
import sys
import requests

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

URL = "http://104.197.194.54:9002/access_ways"
pset = gp.PrimitiveSetTyped("MAIN",[float, float, float, float, float, float, float, float, float], float )
#pset.addPrimitive(operator.and_, [bool, bool], bool)
#pset.addPrimitive(operator.or_, [bool, bool], bool)
#pset.addPrimitive(operator.not_, [bool], bool)

def protectedDiv(left, right):
    try: return left / right
    except ZeroDivisionError: return 1

pset.addPrimitive(operator.add, [float,float], float)
pset.addPrimitive(operator.sub, [float,float], float)
pset.addPrimitive(operator.mul, [float,float], float)
pset.addPrimitive(protectedDiv, [float,float], float)
pset.renameArguments(ARG0='priority')
pset.renameArguments(ARG1='maxSpeed')
pset.renameArguments(ARG2='pollutionGrade')
pset.renameArguments(ARG3='numberOfLanes')
pset.renameArguments(ARG4='distance')
pset.renameArguments(ARG5='speedLimit')
pset.renameArguments(ARG6='conjestion')
pset.renameArguments(ARG7='cleanFactor')
pset.renameArguments(ARG8='open')

def if_then_else(input, output1, output2):
    if input: return output1
    else: return output2

#pset.addPrimitive(operator.lt, [float, float], bool)
#pset.addPrimitive(operator.eq, [float, float], bool)
#pset.addPrimitive(if_then_else, [bool, float, float], float)

pset.addEphemeralConstant("rand100", lambda: random.random() * 100, float)
#pset.addTerminal(False, bool)
#pset.addTerminal(True, bool)

creator.create("FitnessMax", base.Fitness, weights=(-1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=1, max_=3)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("compile", gp.compile, pset=pset)

def evalind(individual):
  return True
    
toolbox.register("evaluate", evalind)
toolbox.register("select", tools.selDoubleTournament, fitness_size=5,parsimony_size=1.3,fitness_first=True)
toolbox.register("mate", gp.cxOnePoint)
toolbox.register("expr_mut", gp.genHalfAndHalf, min_=0, max_=3)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

toolbox.decorate("mate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))
toolbox.decorate("mutate",gp.staticLimit(key=operator.attrgetter("height"),max_value=40))

def evaluate(priority, maxSpeed, pollutionGrade, numberOfLanes, distance, speedLimit, conjestion,  cleanFactor, open):
    return func(priority, maxSpeed, pollutionGrade, numberOfLanes, distance, speedLimit, conjestion, cleanFactor, open)

def getHighWayValue(highWay):
    highWayValue = 0
    if highWay == "residential":
        highWayValue = 8
    elif highWay == "living_street":
        highWayValue = 6
    elif highWay == "tertiary":
        highWayValue = 4
    elif highWay == "primary_link":
        highWayValue = 2
    elif highWay == "primary":
        highWayValue = 1
    return highWayValue

def getSidewalkValue(sideWalk):
    sideWalkValue = 0 
    if sideWalk == "left":
        sideWalkValue = 1
    elif sideWalk == "right":
        sideWalkValue = 1
    elif sideWalk == "both":
        sideWalkValue = 1
    return sideWalkValue

def normaliseData(data):
    for i in range(len(data)):
        data[i] = (data[i] - min(data))/(max(data)-min(data))
    return(data)

def getNumberOfWays():
    get = requests.get(url = URL)
    numberOfWays = len(get.json())
    return numberOfWays

if __name__ == "__main__":
    numberOfWays = getNumberOfWays()
    originalWeights = [0] * numberOfWays
    with open("Individual", "rb") as file:
        individual = pickle.load(file)
        tree=gp.PrimitiveTree(individual)
        print(str(tree))
    func = toolbox.compile(expr=individual)
    reading=True
    while reading:
        for way in range (0, numberOfWays):
            GETPARAMS = {"column_name":"way_id", "value":way}
            get = requests.get(url = URL, params = GETPARAMS)
            wayList = (get.json())[0]
            buildingDensity, roadLength, lanes, speedLimit, highWay, sideWalk  = wayList [7], wayList[8], wayList[9], wayList[10], wayList[11], wayList[12]
            buildingDensity = (10 * buildingDensity) / 127760
            highWayValue = getHighWayValue(highWay)
            sideWalkValue = getSidewalkValue(sideWalk)
            congestion = (10 - highWayValue) * random.uniform(0, 1) 
            cleanFactor = buildingDensity * (highWayValue + sideWalkValue)
            weight = (evaluate(10, speedLimit, 5, lanes, roadLength, 80, congestion, cleanFactor, True))
            originalWeights[way] = weight
            #print("WayID : {}, SpeedLimit : {}, Lanes : {}, RoadLength : {}, Congestion : {}, CleanFactor : {}, Weight : {}".format(way, speedLimit, lanes, roadLength, congestion, cleanFactor, weight))
        break
    #print(originalWeights)
    newWeights = normaliseData(originalWeights)
    #print(newWeights)
    for i in range(len(newWeights)):
        PUTPARAMS = {"set_col":"weighting", "set_val":newWeights[i], "cond_col":"way_id", "con_val":i}
        put = requests.put(url = URL, params = PUTPARAMS)
        #print(put.json())
    
"""
Evaluation variable definitions:
Priority: 1-100, decided by council
MaxSpeed : max speed of vehicle, in mph
PollutionGrade: 1-10, 10 being the worst, decided by some car people
Number of lanes: obtained from an OSM way
Distance: obtained from an OSM way's road_length
Speed limit: obtained from an OSM way's maxspeed
Congestion: value taken from OSM way's highway value and randomised
CleanFactor: determined from OSM way's highway, sidewalk value and building density
Open: is set to True if road is open, False if closed
"""
